#!/usr/bin/env python
# -*- coding: utf-8 -*- 

from wtforms import BooleanField, TextField, PasswordField, validators
from flask.ext.wtf import Form
class LoginForm(Form):
    username = TextField(u'მომხ. სახელი', [validators.Length(min=4, max=25,
    message=u'სახელი უნდა იყოს 4სა და 25ს სიმბოლოს შორის')])
    password = PasswordField(u'პაროლი', [validators.Required(message=u'სავალდებულოა')])

class RegistrationForm(Form):
    username = TextField(u'მომხ. სახელი', [validators.Length(min=4, max=25,
    message=u'სახელი უნდა იყოს 4სა და 25ს სიმბოლოს შორის')])
    email = TextField(u'ელექტრ. ფოსტა', [validators.Email(message=u'არასწორი ელ.ფოსტა') ])
    password = PasswordField(u'პაროლი', [
        validators.Required(message=u'სავალდებულოა'),
        validators.EqualTo('confirm', message=u'პაროლები უნდა ემთხვეოდეს'),
        validators.Length(min=4,
            message=u'სახელი უნდა იყოს მინიმუმ 4 სიმბოლო')
    ])
    confirm = PasswordField(u'გაიმეორეთ პაროლი')

