import settings
from bulbs.neo4jserver import Graph, Config, NEO4J_URI
from models import User, Follows, Bet, Owns
from werkzeug.security import generate_password_hash, \
     check_password_hash
from datetime import datetime
def main():

    config = Config(settings.NEO4J_URI, settings.NEO4J_USER, settings.NEO4J_PASS)
    graph = Graph(config)
    #graph=Graph()
    graph.add_proxy("user",User)
    graph.add_proxy("follows", Follows)
    graph.add_proxy("owns", Owns)
    graph.add_proxy("bet", Bet)
    graph.clear()
    for i in range(10):
        usr = graph.user.create(name="user"+str(i),
            surname="user"+str(i)+"surname",
            e_mail="user"+str(i)+"@gmail.com",
            username="username"+str(i),
            password=generate_password_hash(
                "password"+str(i)))
       

    print "populated "+str(len(graph.V))+ " users!"
    #print graph.V[0]
    for i in range(8):
        graph.follows.create(graph.V[i],graph.V[i+1])

    graph.follows.create(graph.V[2],graph.V[1])
    graph.follows.create(graph.V[2],graph.V[7])
    graph.follows.create(graph.V[7],graph.V[1])
    graph.follows.create(graph.V[5],graph.V[4])
    graph.follows.create(graph.V[9],graph.V[1])
    graph.follows.create(graph.V[1],graph.V[9])
    graph.follows.create(graph.V[6],graph.V[9])
    graph.follows.create(graph.V[1],graph.V[3])
    graph.follows.create(graph.V[3],graph.V[5])
    graph.follows.create(graph.V[7],graph.V[9])
    graph.follows.create(graph.V[7],graph.V[3])
    graph.follows.create(graph.V[7],graph.V[0])
    graph.follows.create(graph.V[9],graph.V[0])
    graph.follows.create(graph.V[9],graph.V[4])
    graph.follows.create(graph.V[9],graph.V[6])
    graph.follows.create(graph.V[5],graph.V[0])


    own1 = graph.owns.create(graph.V[2], graph.bet.create(text = 'moigebs shalva',
        reveal_date =  datetime.strptime(r'2012/11/12 21:00',r'%Y/%m/%d %H:%M')))
    
    print own1.eid
    

if __name__ == '__main__':
    main()
