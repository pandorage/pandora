from bulbs.model import Node, Relationship
from bulbs.property import String, Integer, DateTime
from bulbs.utils import current_datetime
from werkzeug.security import generate_password_hash, \
     check_password_hash

class User(Node):

    element_type = "user"
    name = String(nullable=True)
    surname = String(nullable=True)
    e_mail = String(nullable=False,unique=True, indexed=True)
    username = String(nullable=False,unique=True, indexed=True)
    password = String(nullable=True)
    registration_date = DateTime(default=current_datetime, nullable=False)
    birth_date = DateTime(nullable=True)
    isActive = Integer(default=1,nullable=False)
    fbid = String(nullable=True, unique=True, indexed=True)
    fbtoken = String(nullable=True)
    img_url_large = String(nullable=True, default=r'/static/img/defbig.jpg')
    img_url_small = String(nullable=True, default=r'/static/img/defsmall.jpg')


    #loginistvis damatebuli
    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def is_authenticated(self):
        return True

    def is_active(self):
        return self.isActive

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.eid)

    def __repr__(self):
        return '<User %r>' % (self.username)


class Bet(Node):
    element_type = "Bet"
    text = String(nullable=False)
    creation_time = DateTime(default=current_datetime,nullable=False)
    reveal_date = DateTime(nullable=False)
    shared_on_fb = Integer(default=1,nullable=False)
    isActive = Integer(default=1,nullable=False)

class Follows(Relationship):
    label = "Follows"
    created = DateTime(default=current_datetime, nullable=False)
    isActive = Integer(default=1,nullable=False)



class Owns(Relationship):
    label = "Owns"
    creation_time=DateTime(default=current_datetime, nullable=False)
    isActive = Integer(default=1,nullable=False)


class Opposes(Relationship):
    label="Oposes"
    creation_time = DateTime(default=current_datetime, nullable=False)
    isActive = Integer(default=1,nullable=False)
    
