#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import settings
from flask import Flask, redirect, url_for, session, request, g , \
    render_template, flash, abort
from models import User, Follows,Bet,Owns

from bulbs.neo4jserver import Graph, Config, NEO4J_URI
from flask_oauth import OAuth
from forms import LoginForm, RegistrationForm
from flask.ext.login import login_user, logout_user, current_user, \
        login_required, LoginManager
from werkzeug.security import generate_password_hash, \
     check_password_hash

from datetime import datetime
import itertools

app = Flask(__name__)
app.secret_key = settings.SECRET_KEY
app.debug = False
oauth = OAuth()

login_manager = LoginManager()
login_manager.init_app(app)
graph = Graph(Config(NEO4J_URI, settings.NEO4J_USER,
            settings.NEO4J_PASS))
#graph = Graph()
graph.add_proxy("user",User)
graph.add_proxy("follows",Follows)
graph.add_proxy("bet",Bet)
graph.add_proxy("owns",Owns)

facebook = oauth.remote_app('facebook',
    base_url='https://graph.facebook.com/',
    request_token_url=None,
    access_token_url='/oauth/access_token',
    authorize_url='https://www.facebook.com/dialog/oauth',
    consumer_key=settings.FACEBOOK_APP_ID,
    consumer_secret=settings.FACEBOOK_APP_SECRET,
    request_token_params={'scope': 'email'}
)

@app.before_request
def before_request():
    g.user = current_user
    if g.user.is_authenticated():
        g.gruser = graph.user.get(g.user.eid)

@app.teardown_request
def teardown_request(exception):
    pass

@app.route("/")
def index():
    if g.user.is_active():
        return redirect('/profile/'+g.user.username)
 
    return redirect(url_for('login'))

@app.route('/profile/<req_username>')
def profile(req_username):
    req_user = graph.user.index.lookup(username=req_username)
    if not req_user:
        abort(404)
    req_user = req_user.next()

    if g.user.is_authenticated() and req_user.username == g.user.username:

        # tavis profilzea shesuli 
        follows = g.gruser.outV('Follows')
        if not follows:
            follows = []
        else:
            follows = [usr for usr in follows]
        suggestions = get_suggestions(follows, mx=10)
        revealed_bets = get_bets(usrs=follows)
        revealed_bets += get_bets(usrs = [g.gruser])
        unrevealed_bets = get_bets(usrs=[g.gruser], revealed=False)
        bet_list = revealed_bets + unrevealed_bets
        bet_list = sorted(bet_list, key=lambda x: x.creation_time,reverse=True)

        return render_template('profile.html', user=g.user,bets=bet_list,
                sugs=suggestions)
    else:
        bet_list = [bet for bet in get_bets(usrs= [req_user], revealed=True)]
        bet_list = sorted(bet_list,key=lambda x: x.creation_time)
        return render_template('alienprofile.html', user=req_user, 
                bets=bet_list)

@app.route('/about')
def about():
    return render_template('about.html')
@app.route('/wtf')
def wtf():
    return render_template('wtf.html')

@app.route('/profile/<parameter_username>/<parameter_option>')
def show_user_profile(parameter_username,parameter_option="/"):
    # show the user profile for that user, followers and follows
    if graph.user.index.lookup(username=parameter_username)==None:
        return "user %s is not exist" % parameter_username 

    if parameter_option=="followers":
        text = "followers of <b>"+parameter_username+"</b> are</br>"
        node=None
        for vertice in graph.V:
            if vertice.username==parameter_username:
                node=vertice
        for publisher in node.inV("Follows"):
            text+=publisher.username+"</br>"

        return text

    if parameter_option=="follows":
        text = "user <b>"+parameter_username+"</b> follows to</br>"
        node=None
        for vertice in graph.V:
            if vertice.username==parameter_username:
                node=vertice
        for follower in node.outV("Follows"):
            text+=follower.username+"</br>"

        return text

    return 'User %s and parameter %s' % (parameter_username,parameter_option)

    def get_followers(usrname):
        pass

    def get_follows(usrname):
        pass

# abrunebs usr list is betebs tu revealed aris True mashin reveald ebs
# tu arada unrevealdebs
def get_bets(mx=12, usrs=[], revealed=True):
    
    if not usrs:
        return []
    bets = []
    for usr in usrs:
        candidates = usr.outV('Owns')
        if not candidates:
            continue
        for bet in candidates:
            if len(bets) == mx:
                return bets
            if (revealed and bet.reveal_date <= datetime.now()) or \
                (not revealed and bet.reveal_date > datetime.now()):
                bets.append(bet)
                bets[-1].revealed = revealed 
                bets[-1].placer = usr
                
    return bets

def get_suggestions(follows, mx=10):
    all_usrs = graph.user.get_all()
    fb_reg_usrs = set(usr for usr in all_usrs if usr.fbid)
    if not follows:
        follows = []
    ret = []
    xx = set(usr.fbid for usr in follows)
    for fbu in fb_reg_usrs:
        if fbu.fbid not in xx and fbu.fbid != g.gruser.fbid:
            ret.append(fbu)
        if len(ret) == mx:
            break
    return ret

@app.route('/exxxx' , methods=['POST'])
@login_required
def exxxx():
    #graph.bet.create(text=)
    bet_text = request.form["text"]
    bet_reveal_date=request.form["date"]
    cur_bet=graph.bet.create(text=bet_text, reveal_date=datetime.strptime(bet_reveal_date,r'%Y/%m/%d %H:%M'),creation_time=datetime.now())
    #import ipdb; ipdb.set_trace() # BREAKPOINT
    owns_rel=graph.owns.create(graph.user.get(g.user.eid),cur_bet)
    return str(cur_bet.eid)+" "+str(owns_rel.eid)

@login_manager.user_loader
def load_user(id):
    return  graph.user.get(int(id))


@app.route('/register',methods=['GET','POST'])
def register():
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('index'))
    form = RegistrationForm()
    
    if form.validate_on_submit():
        def try_to_register():
            usr = graph.user.index.lookup(username=form.username.data)
            if usr:
                return u'ასეთი მომხმარებელი უკვე არსებობს'
            usr = graph.user.index.lookup(e_mail=form.email.data)
            if usr:
                return u'ასეთი ელ.ფოსტა უკვე არსებობს'

            usr = graph.user.create(e_mail=form.email.data,
                    username=form.username.data,
                    password=generate_password_hash(form.password.data))

        err = try_to_register()
        if err:
            flash(err)
            return redirect(url_for('register'))
        else:
            flash(u'თქვენ დარეგისტრირდით წარმატებით, გთხოვთ გაიაროთ ავტორიზაცია')
            return redirect(url_for('login'))

    return render_template('register.html',form=form,title='Sign in')


@app.route('/login', methods=['GET', 'POST'])
def login():
    
    form = LoginForm()

    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('index'))
    if form.validate_on_submit():
            # login gadasatania calke

        def try_to_login():
            usr = graph.user.index.lookup(username=form.username.data)
            if not usr:
                return u'მოხმ. სახელი არასწორია'
            usr = usr.next()

            if not usr.check_password(form.password.data):
                return u'პაროლი არასწორია'
            login_user(usr)

        err = try_to_login()
        if (err):
            flash(err)
            return redirect(url_for('login'))
        else:
            return redirect(request.args.get("next") or url_for("index"))
                


    return render_template('login.html',form=form,title=u'Log in')


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/loginfb')
def loginfb():
    return facebook.authorize(callback=url_for('facebook_authorized',
        next=request.args.get('next') or request.referrer or None,
        _external=True))


@app.route('/login/authorized')
@facebook.authorized_handler
def facebook_authorized(resp):
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        )
    
    session['oauth_token'] = (resp['access_token'], '')
    me = facebook.get('/me')

    # my login 
    def try_to_loginorregister():
            fbusername = me.data['username']

            usr = graph.user.index.lookup(username=fbusername)
            if usr:
                usr = usr.next()

                if hasattr(usr, 'fbid'):
                    # user already registered
                    login_user(usr)
                    return
                fbusername += '1'
              
            usr = graph.user.create(e_mail=me.data['email'],
                    username=fbusername, name=me.data['first_name'],
                    surname=me.data['last_name'],fbid=me.data['id'],
                    fbtoken=get_facebook_oauth_token()[0],
                    img_url_large='http://graph.facebook.com/' + me.data['id']
                    + '/picture?type=large',
                    img_url_small='http://graph.facebook.com/' + me.data['id']
                    + '/picture')
            login_user(usr)

     
    err = try_to_loginorregister()
    if err:
        flash(err)
        redirect(url_for('index'))
    #import ipdb; ipdb.set_trace() # BREAKPOINT
    return redirect(url_for('index'))
            #return redirect(request.args.get("next") or url_for("index"))


@facebook.tokengetter
def get_facebook_oauth_token():
    return session.get('oauth_token')


@app.errorhandler(404)
def page_not_found(error):
    return 'aseti gverdi araa'
    #return render_template('page_not_found.html'), 404
@app.route('/feedtemplate')
def alientemplate():
    return render_template('feedtemplate.html')

@app.route('/follow_to' , methods=['POST'])
@login_required
def follow_to():
    
    usrname = request.form["username"]
    follow_rel=graph.follows.create(g.gruser,graph.user.index.lookup(username=usrname).next())
    return str(follow_rel.eid)
    #return "python follow_to funqcias gadaeca "+ usrname+" ogond grapshi ar sheqmna jer(specialurad)"

if __name__ == '__main__':
    app.run()
